# GMT class 2016

This repository constains the lecture slides and additional material of the tutorials

## Day 1
* Monday lecture slides, [morning](/Monday_Morning.pdf) and [afternoon](/Monday_Afternoon.pdf)

## Day 2
* [Tuesday lecture slides](/Tuesday.pdf)
* [Plate boundary exercise (no.01)](/Tuesday_ex1/Ex1.md) and all [relevant files](/Tuesday_ex1/)
* [Seismicity & shaded relief (Italy exercise)](/Tuesday_ex2/Ex_Tuesday_Italy.sh)

## Day 3
* [Wednesday lecture slides](/Wednesday.pdf)
* [volcano locations and p-wave tomography (Japan exercise) ](/Wednesday_ex1/Ex_Wednesday_Japan.sh)

## Day 4
