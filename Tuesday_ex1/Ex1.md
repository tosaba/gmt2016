[Home](/README.md) | [Scripts & data](/Tuesday_ex1) | [Data reference][1] 

[1]: http://peterbird.name/oldFTP/PB2002/PB2002_boundaries.dig.txt "Peter Bird's webpage"

# Processing

The original file has been downloaded from [Peter Bird's webpage][1]

Remove lines with leading "\*"
``` bash
sed '/^*/d' PB2002_boundaries.dig.txt > PB.xy
```

Add GMT delimiter ">" for polygon segments
```bash
sed '/^[A-Z]/s/^/> /'  PB.xy > PB_clean.xy
```

Replace "," with " " (space)
```bash
sed 's/,/ /g' PB_clean.xy > PB_cleaner.xy
```

# Plot

Define variables
```bash
psfile=plateboundaries.ps
geom=-Rg
proj=-JKf25c
```

Plot continents on a global map
```bash
gmt pscoast $geom $proj -Glightgray -A1000 -Swhite -Ba45f10 -B+t"Plate boundaries" -K > $psfile
```
Plot plate boundaries with red and no symbols (finalize the map with "-O" only)
```bash
gmt psxy PB_cleaner.xy $geom $proj -Wthin,red -O >> $psfile
```

Transform postscript into pdf
```bash
ps2raster -Tf $psfile
```
