#!/bin/bash
# --------------------------------------------------------------------
# Tutorial on how to plot plate boundaries published by Bird(2003)
#
# GMT version 5.1.2
#
# Tobias S. Baumann, Mainz, 2016
# --------------------------------------------------------------------

# === Processing ===

# (1) The original file has been downloaded from
# http://peterbird.name/oldFTP/PB2002/PB2002_boundaries.dig.txt

# (2) Remove lines with leading "*"
sed '/^*/d' PB2002_boundaries.dig.txt > PB.xy

# (3) Add GMT delimiter ">" for polygon segments
sed '/^[A-Z]/s/^/> /'  PB.xy > PB_clean.xy

# (4) Replace "," with " " (space)
sed 's/,/ /g' PB_clean.xy > PB_cleaner.xy


# === Plot ==
psfile=plateboundaries.ps
geom=-Rg
proj=-JKf25c

# (5) Plot continents on a global map
gmt pscoast $geom $proj -Glightgray -A1000 -Swhite -Ba45f10 -B+t"Plate boundaries" -K > $psfile

# (6) Plot plate boundaries with red and no symbols (finalize the map with "-O" only)
gmt psxy PB_cleaner.xy $geom $proj -Wthin,red -O >> $psfile 

# (7) Transform postscript into pdf
ps2raster -Tf $psfile
