#!/bin/bash
# --------------------------------------------------------------------------------
# GMT class 2016
# Exercise:  No. 02
# Plot gray shaded topography and depth-dependent seismicity
#
# GMT version 5.1.2
#
# Tobias S. Baumann, Mainz, 2016
# --------------------------------------------------------------------------------

# data
topo=../ETOPO1_Ice_g_gdal.grd  # path to global etopo1 file
                               # downloaded from https://www.ngdc.noaa.gov/mgg/global/relief/ETOPO1/data/ice_surface/grid_registered/netcdf/
seisdat=ex.data                # downloaded from http://www.globalcmt.org/CMTsearch.html
platebnds=PB_cleaner.xy        # Peter Bird plate boundary data (pre-processed, see ex.01)
                               # downloaded from  http://peterbird.name/oldFTP/PB2002/PB2002_boundaries.dig.txt

# variables
psfile=italy.ps
geom=-R5/20/35/50
proj=-JM15c


# === Processing ===

# cut topography
gmt grdcut $topo $geom -Gtopo_cut.grd

# gradient of topography grid
gmt grdgradient topo_cut.grd -A0 -Nt -Gtopo_grad.grd

# topography colormap (run grdinfo topo_cut.grd to determine data extends)
gmt makecpt -T-5000/5000/100 -Cgray > topo.cpt

# seismicity depth colormap (run gmtinfo $seisdat to get depth extends)
gmt makecpt -T0/300/5 -D -Cpolar > seisdepth.cpt

# === Plot ===

# topo
gmt grdimage topo_cut.grd $geom $proj -Ctopo.cpt -Itopo_grad.grd -P -Y5c -K> $psfile

# coastlines (-L legend/scale)
gmt pscoast $geom $proj -Df -Wthin,black -A500 -Lfx2.5/19/45/300+l -K -O >> $psfile
# plate boundaries
gmt psxy $platebnds $geom $proj -Wtomato,thick-- -K -O >> $psfile

# depth dependent seismicity
gmt psxy $seisdat  $geom $proj -Bf1a5g5 -Sc0.25 -Cseisdepth.cpt -Wthin,darkblue -K -O >> $psfile

# colorbar-Bg0.2f0.1a0.2:"Orientation error":/:"[1]":
gmt psscale -D7.5/-1/10/0.5h -Cseisdepth.cpt -Ef  -Ba50g50f10:"Depth of seismicity":/:"[km]": -O >> $psfile

# transform ps into pdf
gmt ps2raster -Tf $psfile
