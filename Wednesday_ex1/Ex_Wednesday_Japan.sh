# --------------------------------------------------------------------------------
# GMT class 2016
# Exercise:  No. 01 - Wednesday
# Plot grid data and contourlines of bouguer and velocity
#
# GMT version 5.1.2
#
# Tobias S. Baumann, Mainz, 2016
# --------------------------------------------------------------------------------

# data
vpdat=../Wednesday_ex1_data/vp.xyz
bougdat=../Wednesday_ex1_data/boug.xyz
volcdat=../Wednesday_ex1_data/volcanoes.xy

# variables
psfile=japan.ps
geom=-R138/142/37/42
proj=-JM15c

# === Processing ===
# interpolate into grd file
gmt surface $geom -I0.1 $vpdat -Gvp.grd

# create vp color map (check z space with grdinfo to set color ranges)
gmt makecpt -Chaxby -T7.2/8.2/0.05 -V > vp.cpt

# create map that is used for contours only
gmt makecpt -Chaxby -T7/8/0.2 -V > vp_c.cpt


# === Plot ===

# plot grd file
#gmt grdimage $vpdat $geom $proj -Cvp.cpt -P -E100 -Y2 -K > $psfile
gmt grdimage vp.grd $geom $proj -Cvp.cpt -P -Y2 -K > $psfile

# plot contour lines of grd file
gmt grdcontour vp.grd $geom $proj -Cvp_c.cpt -A0.1 -Wthin -O -K  >> $psfile

# plots volcano locations
gmt psxy $volcdat $geom $proj -St0.085i -Gblack  -K -O >> $psfile

# add shorelines and overlay offshore areas
gmt pscoast $geom $proj -B2g1 -Df -Wthin -Slightgray -Lfx3/23/10/100+l -K -O >> $psfile

# add colorbar
gmt psscale -D3/22/5/0.4h -Cvp.cpt -Bg0.2a0.2f0.1:"Vp-velocity":/:"km/s": -O >> $psfile

# turn ps into pdf
gmt ps2raster -Tf $psfile
